
/////////////////GLOBALS & GAMESTATE ///////////////////////
//canvas.width = window.innerWidth - 15;
//canvas.height = window.innerHeight / 1.4;

document.body.style.overflow = 'hidden';

const Menu = 0;
const Collection = 1;
const Battle = 2;

let State = Menu;

const ASSETS = "js/game/assets/";

//////////////////MEMBER VARIABLES/////////////////////////

// collection order: sasuke, otherguy ...
let collectionArray = [false, false];
let dateOption = 0;

const menu_battleButton = new uiButton(900,150,300,50,"white", "1) Battle", "black");
const menu_collectionButton = new uiButton(900,250,300,50,"white", "2) Collection", "black");

const sasuke = new uiImage(100,100, ASSETS + "sasuke.png", 500, 683);
const otherguy = new uiImage(100,100, ASSETS + "sasuke.png", 500, 683);

const dialogueBox = new uiDialogue(50,550,700,150);

//////////////////MEMBER FUNCTIONS///////////////////////////

function replaceAt(index, msg, str)
{
    var s = str;
    s = s.substring(0, index) + msg + s.substring(index + 1);
    return s;  
}

function setSrcs()
{
    if (!collectionArray[0]) // sasuke
    {
        sasuke.setSrc(ASSETS + "sasuke_gray.png");
    }
    else
    {
        sasuke.setSrc(ASSETS + "sasuke.png");
    }
    if (!collectionArray[1]) // sasuke
    {
        otherguy.setSrc(ASSETS + "sasuke_gray.png");
    }
}

function setDate(date)
{
    for (let i = 0; i < collectionArray.length; i++)
    {
        setIndexActive(i, false);
        if (i == date)
        {
            setIndexActive(i, true);
        }
    }
}

function setIndexActive(index, active)
{
    if (index == 0)
    {
        sasuke.setActive(active);
    }
    else if (index == 1)
    {
        otherguy.setActive(active);
    }
}

function load()
{
    // collection order: sasuke ...
    let s = localStorage.getItem(localStorage.getItem("activeID") + "Data");
    s = replaceAt(s.length - 1, '', s);
    let ssplit = [];
    if (s.length > 2)
    {
        ssplit = s.split(",");
    }
    else
    {
        ssplit = [s];
    }
    for (let i = 0; i < ssplit.length; i++)
    {
        if (ssplit[i] == "t")
        {
            collectionArray[i] = true;
        }
        else
        {
            collectionArray[i] = false;
        }
    }
    setSrcs();
    setDate(localStorage.getItem(localStorage.getItem("activeID") + "DataOption"));
}
load(); // load everything that needs to be saved
window.requestAnimationFrame(GameLoop);
function GameLoop()
{
    Update();
    Draw();
    window.requestAnimationFrame(GameLoop);
}

function Update()
{
    switch(State)
    {
        case Menu:
            menu_update();
            break;
        case Collection:
            break;
        case Battle:
            battle_update();
            break;
    }
}

function Draw()
{
    ctx.clearRect(0, 0, canvas.width, canvas.height); //CLEAR SCREEN
    switch(State)
    {
        case Menu:
            menu_draw();
            break;
        case Collection:
            break;
        case Battle:
            battle_draw();
            break;
    }
}

document.addEventListener('keydown', keydown);
function keydown(event)
{
    var key = event.key;

    switch(State)
    {
        case Menu:
            menu_processEvents(key);
            break;
        case Collection:
            break;
        case Battle:
            battle_processEvents(key);
            break;
    }
}

function buttondown(key)
{
    switch(State)
    {
        case Menu:
            menu_processEvents(key);
            break;
        case Collection:
            break;
        case Battle:
            battle_processEvents(key);
            break;
    }
}


window.onbeforeunload = function()
{
    let s = "";
    for(let i = 0; i < collectionArray.length; i++)
    {
        if (collectionArray[i] == true)
        {
            s = s + "t,";
        }
        else
        {
            s = s + "f,";
        }
    }
    localStorage.setItem(localStorage.getItem("activeID") + "Data", s);
    localStorage.setItem(localStorage.getItem("activeID") + "DataOption", dateOption);
}