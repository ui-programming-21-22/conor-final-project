var nameText;

function EnterName()
{
    nameText = document.getElementById("nameID").value;
    if (nameText.length > 3)
    {
        if (localStorage.getItem(nameText) == null)
        {
            ConfirmRegister();
        }
        else
        {
            ConfirmLogin();
        }
    }   
    else
    {
        document.getElementById("warningbr").style.display = "inline-block";
        document.getElementById("warning").innerHTML = "Please enter more characters!";
    }
}

function ConfirmRegister()
{
    document.getElementById("finish").style.display = "inline-block";
    document.getElementById("warningbr").style.display = "none";
        document.getElementById("warning").innerHTML = "";
}

function ConfirmLogin()
{
    localStorage.setItem("activeID", nameText);
    ChangeLink();
}

function FinishRegister()
{
    console.log(nameText);
    localStorage.setItem(nameText, nameText);
    localStorage.setItem("activeID", nameText);
    localStorage.setItem(nameText + "Data", "f, f,");
    
    ChangeLink();
}

function ChangeLink()
{
    var str = window.location.pathname;
    str = str.split("index.html");
    window.location.pathname = str[0] + "game.html";
}