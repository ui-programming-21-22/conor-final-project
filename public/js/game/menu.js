
function menu_setText()
{
    if (sasuke.getActive())
    {
        if (!collectionArray[0]) dialogueBox.setText("Huh, whadya want?");
        else dialogueBox.setText("Hey, how are you?");
    }
    else if (otherguy.getActive())
    {
        if (!collectionArray[1]) dialogueBox.setText("Oh I didn't see you there");
        else dialogueBox.setText("You know, you're pretty cute");
    }
}

function menu_processEvents(key)
{
    if (key == '1')
    {
        State = Battle;
    }
    if (key == '2')
    {
        State = Collection;
    }
}

function menu_update()
{
    menu_setText();
}

function menu_draw()
{
    menu_battleButton.draw();
    menu_collectionButton.draw();
    sasuke.draw();
    otherguy.draw();
    
    dialogueBox.draw();
}