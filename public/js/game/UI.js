const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");

class uiButton
{
    constructor (left = 0, top = 0, width = 0, height = 0, colour = "white", text = "Sample", textColour = "black")
    {
        this.top = top;
        this.left = left;
        this.width = width;
        this.height = height;
        this.colour = colour;
        this.text = text;
        this.textColour = textColour;
        this.active = true;
    }

    setPosition(x, y)
    {
        this.top = x;
        this.left = y;
    }

    setSize(w, h)
    {
        this.width = w;
        this.height = h;
    }

    setColour(colour)
    {
        this.colour = colour;
    }

    setTextColour(colour)
    {
        this.textColour = colour;
    }

    setText(text)
    {
        this.text = text;
    }

    setActive(bool)
    {
        this.active = bool;
    }

    draw()
    {
        if (this.active)
        {
            ctx.fillStyle = this.colour;
            ctx.fillRect(this.left, this.top, this.width, this.height);
            ctx.fillStyle = this.textColour;
            ctx.font = '30px Comic Sans MS';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText(this.text, this.left + this.width/2, this.top + this.height/2);
        }
    }

}

class uiImage
{
    constructor (x = 0, y = 0, src = "", width, height)
    {
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.srcImage = new Image(0,0);
        this.srcImage.src = src;
        this.active = true;
        this.sx = 0;
        this.sy = 0;
    }

    setPosition(x, y)
    {
        this.x = x;
        this.y = y;
    }

    setSrc(src)
    {
        this.srcImage.src = src;
    }

    setActive(bool)
    {
        this.active = bool;
    }

    getActive()
    {
        return this.active;
    }

    setSelection(x,y)
    {
        this.sx = x;
        this.sy = y;
    }

    draw()
    {
        if (this.active)
        {
            //ctx.drawImage(this.srcImage, this.x, this.y);
            ctx.drawImage(this.srcImage, this.sx, this.sy, this.width, this.height, this.x, this.y, this.width, this.height)
        }
    }
}

class uiDialogue
{
    constructor (x = 0, y = 0, width = 0, height = 0)
    {
        this.y = y;
        this.x = x;
        this.width = width;
        this.height = height;
        this.active = true;
    }

    setPosition(x, y)
    {
        this.x = x;
        this.y = y;
    }

    setText(text)
    {
        this.text = text;
    }

    setActive(bool)
    {
        this.active = bool;
    }

    draw()
    {
        if (this.active)
        {
            ctx.fillStyle = "white";
            ctx.fillRect(this.x, this.y, this.width, this.height);
            ctx.fillStyle = "black";
            ctx.font = '30px Comic Sans MS';
            ctx.textAlign = 'left';
            ctx.textBaseline = 'middle';
            ctx.fillText(this.text, this.x + this.width/30, this.y + this.height/6);
        }
    }
}