

//////////////////MEMBER VARIABLES/////////////////////////

const battle_fight = new uiButton(900,150,300,50,"white", "1) FIGHT", "black");
const battle_run = new uiButton(900,250,300,50,"white", "2) RUN", "black");

const battle_fight_talk = new uiButton(900,100,300,50,"white", "1) Talk", "black");
const battle_fight_hug = new uiButton(900,200,300,50,"white", "2) Hug", "black");
const battle_fight_flirt = new uiButton(900,300,300,50,"white", "3) Flirt", "black");
const battle_fight_kiss = new uiButton(900,400,300,50,"white", "4) Kiss", "black");
const battle_fight_play = new uiButton(900,500,300,50,"white", "5) Play", "black");
const battle_fight_BACK = new uiButton(900,600,300,50,"white", "6) Back", "black");

let fightOptions = false;
let initial = false;

let won = false;

///////////////////////////////////////////////////////////

function battle_setText()
{
    
    initial = true;
    if (sasuke.getActive())
    {
        dialogueBox.setText("This is going to be so boring");
    }
    else if (otherguy.getActive())
    {
        dialogueBox.setText("Aren't you a little nervous?");
    }
}

function battle_processEvents(key)
{
    if (!fightOptions)
    {
        if (key == '1')
        {
            fightOptions = true;
        }
        if (key == '2')
        {
            fightOptions = false;
            State = Menu;
        }
    }
    else
    {
        if (key == '1')
        {
            collectionArray[0] = true;
            setSrcs();
            sasuke.setSelection(500,0);
            won = true;
        }
        if (key == '6')
        {
            fightOptions = false;
        }
    }
}

function battle_update()
{
    if (!initial) battle_setText();
    if (won) dialogueBox.setText("I guess you're cute, whatever.");
}

function battle_draw()
{
    if (!fightOptions)
    {
        battle_fight.draw();
        battle_run.draw();
    }
    else
    {
        battle_fight_talk.draw();
        battle_fight_hug.draw();
        battle_fight_flirt.draw();
        battle_fight_kiss.draw();
        battle_fight_play.draw();
        battle_fight_BACK.draw();
    }
    
    sasuke.draw();
    otherguy.draw();

    dialogueBox.draw();
}